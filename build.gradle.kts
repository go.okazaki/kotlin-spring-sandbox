val coroutinesVersion = System.getProperty("coroutinesVersion")

plugins {

  val kotlinVersion = System.getProperty("kotlinVersion")
  java
  `java-library`
  jacoco
  kotlin("jvm") version kotlinVersion
  // https://plugins.gradle.org/plugin/de.jansauer.printcoverage
  id("de.jansauer.printcoverage") version "2.0.0"
  // https://plugins.gradle.org/plugin/org.ec4j.editorconfig
  id("org.ec4j.editorconfig") version "0.0.3"
  // https://plugins.gradle.org/plugin/org.owasp.dependencycheck
  id("org.owasp.dependencycheck") version "5.3.2.1"
  // https://plugins.gradle.org/plugin/net.researchgate.release
  id("net.researchgate.release") version "2.8.1"
}

allprojects {
  group = "sandbox"
  version = project.version

  apply(plugin = "java")
  apply(plugin = "java-library")
  apply(plugin = "jacoco")
  apply(plugin = "kotlin")
  apply(plugin = "de.jansauer.printcoverage")

  repositories {
    jcenter()
    mavenCentral()
  }

  dependencies {
    implementation(kotlin("stdlib"))
    implementation(kotlin("reflect"))
    testImplementation(kotlin("test"))
    testImplementation(kotlin("test-junit"))

    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${coroutinesVersion}")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor:${coroutinesVersion}")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:${coroutinesVersion}")
  }

  tasks {

    withType<JavaCompile> {
      sourceCompatibility = JavaVersion.VERSION_11.toString()
      targetCompatibility = JavaVersion.VERSION_11.toString()
    }

    withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>().configureEach {
      kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
        freeCompilerArgs = listOf("-progressive")
      }
    }

    test {
      useJUnitPlatform()
    }

    jacocoTestReport {
      reports {
        xml.isEnabled = true
        html.isEnabled = true
      }
    }

    printCoverage {
      dependsOn(jacocoTestReport)
      coverageType.set("INSTRUCTION")
    }
  }
}

subprojects {

  dependencies {
    // https://github.com/reactor/reactor
    implementation(platform("io.projectreactor:reactor-bom:Dysprosium-SR8"))
    testImplementation("io.projectreactor:reactor-test")

    testImplementation(platform("org.junit:junit-bom:5.6.2"))
    testImplementation("org.junit.jupiter:junit-jupiter")
    testImplementation("org.assertj:assertj-core:3.16.1")
  }
}

tasks {

//  defaultTasks("clean", "editorconfigCheck", "dependencyCheckAggregate", "assemble")
  defaultTasks("clean", "editorconfigCheck", "assemble")

  editorconfig {
    excludes = listOf(
      "gradle**",
      "**/build/**",
      "**/node_modules/**",
      "**/src/main/resources/static/**"
    )
  }

  dependencyCheck {
    failBuildOnCVSS = 9.0f
    suppressionFile = "suppression.xml"
  }

  release {
    tagTemplate = "v\$version"
  }

  // https://docs.gradle.org/current/samples/sample_jvm_multi_project_with_code_coverage.html
  task("codeCoverageReport", JacocoReport::class) {

    subprojects {
      val subproject = this
      subproject.plugins.withType<JacocoPlugin>().configureEach {
        subproject.tasks.matching { it.extensions.findByType<JacocoTaskExtension>() != null }.configureEach {
          val testTask = this
          sourceSets(subproject.sourceSets.main.get())
          executionData(testTask)
        }

        subproject.tasks.matching { it.extensions.findByType<JacocoTaskExtension>() != null }.forEach {
          rootProject.tasks["codeCoverageReport"].dependsOn(it)
        }
      }
    }

    reports {
      xml.isEnabled = true
      xml.destination = File("${buildDir}/reports/jacoco/test/jacocoTestReport.xml")
      html.isEnabled = true
    }
  }
}
