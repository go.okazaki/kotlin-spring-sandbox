plugins {
  // https://plugins.gradle.org/plugin/com.github.johnrengelman.shadow
  id("com.github.johnrengelman.shadow") version "5.2.0"
}

base {
  archivesBaseName = "sandbox-client"
}

dependencies {

  implementation(project(":domain"))
  testImplementation(project(":domain", "testRuntime"))

  implementation("com.google.code.gson:gson:2.8.6")
}

tasks {

  shadowJar {
    archiveBaseName.set("sandbox-client")
    archiveClassifier.set("")
    archiveVersion.set("")
  }
}
