package sandbox.client

import sandbox.domain.Thing
import sandbox.domain.Things
import java.util.*

object ThingClient : Things {

  override suspend fun findById(id: UUID): Thing {
    TODO("Not yet implemented")
  }

  override suspend fun save(target: Thing): Thing {
    TODO("Not yet implemented")
  }

  override suspend fun delete(target: Thing): Boolean {
    TODO("Not yet implemented")
  }
}
