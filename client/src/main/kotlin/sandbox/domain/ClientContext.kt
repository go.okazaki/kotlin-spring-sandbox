package sandbox.domain

import sandbox.client.ThingClient

object ClientContext : Context {

  init {
    Context.instance = this
  }

  override val things: Things
    get() = ThingClient
}
