package sandbox.client

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import sandbox.domain.ClientContext

internal class ThingClientTest {

  @Test
  fun test() {

    val things = ClientContext.things
    Assertions.assertThat(things).isInstanceOf(ThingClient.javaClass)
  }
}
