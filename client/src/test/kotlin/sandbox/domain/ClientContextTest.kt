package sandbox.domain

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

internal class ClientContextTest {

  @Test
  fun instance() {

    ClientContext
    val instance = Context.instance
    Assertions.assertThat(instance).isInstanceOf(ClientContext.javaClass)
  }
}
