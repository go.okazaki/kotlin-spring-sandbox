plugins {
}

dependencies {
}

tasks {

  val testJar = task("testJar", org.gradle.api.tasks.bundling.Jar::class) {
    archiveClassifier.set("tests")
    from(sourceSets.test.get().output)
  }

  artifacts {
    testRuntime(testJar)
  }

  jar {
    enabled = true
  }
}
