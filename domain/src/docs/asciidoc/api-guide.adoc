= REST API Document
:doctype: book
:icons: font
:source-highlighter: highlightjs
:toc: left
:toclevels: 1
:sectlinks:

== 例

=== Request
include::{snippets}/example/http-request.adoc[]
=== Response
include::{snippets}/example/http-response.adoc[]
=== cURL
include::{snippets}/example/curl-request.adoc[]
