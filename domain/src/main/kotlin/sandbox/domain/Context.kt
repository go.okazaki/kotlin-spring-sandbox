package sandbox.domain

interface Context {

  val things: Things

  companion object {
    lateinit var instance: Context
  }
}
