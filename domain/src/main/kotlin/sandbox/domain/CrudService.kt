package sandbox.domain

interface CrudService<T, ID> {

  suspend fun findById(id: ID): T

  suspend fun save(target: T): T

  suspend fun delete(target: T): Boolean
}
