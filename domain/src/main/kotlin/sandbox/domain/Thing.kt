package sandbox.domain

import java.util.*

interface Thing {
  val thingId: UUID?
  val thingName: String
  val relationIds: MutableSet<UUID>
}

interface Things : CrudService<Thing, UUID> {

}
