package sandbox.event

import java.lang.ref.Cleaner
import java.util.concurrent.ConcurrentHashMap
import java.util.function.Consumer
import java.util.function.Function

class Bus<E> : Iterable<(E) -> Any?>, Cleaner.Cleanable {

  private val functions: MutableMap<Any, (E) -> Any?> = ConcurrentHashMap()

  fun subscribe(func: (E) -> Any?) {
    functions[func] = func
  }

  fun subscribe(func: Function<E, *>) {
    functions[func] = { func.apply(it) }
  }

  fun subscribe(func: Consumer<E>) {
    functions[func] = { func.accept(it) }
  }

  fun unsubscribe(func: (E) -> Any?) {
    functions.remove(func)
  }

  fun unsubscribe(func: Function<E, *>) {
    functions.remove(func)
  }

  fun unsubscribe(func: Consumer<E>) {
    functions.remove(func)
  }

  override fun iterator(): Iterator<(E) -> Any?> {
    return functions.values.iterator()
  }

  override fun clean() {
    functions.clear()
  }
}
