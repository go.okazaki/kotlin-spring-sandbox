package sandbox.event

import kotlinx.coroutines.flow.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.core.publisher.ParallelFlux
import reactor.core.scheduler.Scheduler
import reactor.core.scheduler.Schedulers
import java.util.concurrent.ConcurrentHashMap
import java.util.function.Consumer
import java.util.function.Function
import kotlin.reflect.KClass

object EventBus {

  private val BUSES: MutableMap<Class<*>, Bus<*>> = ConcurrentHashMap()

  @JvmStatic
  fun <E : Any> publishFlux(event: E): Flux<Any?> {
    return Flux.fromIterable(getBus(event.javaClass))
      .flatMap {
        Mono.just(it)
          .map { it.invoke(event) }
          .onErrorResume { Mono.empty() }
      }
  }

  @JvmStatic
  fun <E : Any> publishParallelFlux(event: E, scheduler: Scheduler): ParallelFlux<Any?> {
    return Flux.fromIterable(getBus(event.javaClass))
      .parallel()
      .runOn(scheduler)
      .flatMap {
        Mono.just(it)
          .map { it.invoke(event) }
          .onErrorResume { Mono.empty() }
      }
  }

  @JvmStatic
  fun <E : Any> publishParallelFlux(event: E): ParallelFlux<Any?> {
    return publishParallelFlux(event, Schedulers.elastic())
  }

  suspend fun <E : Any> publishFlow(event: E): Flow<Any?> {
    return getBus(event.javaClass).asFlow()
      .flatMapMerge {
        flow {
          emit(it.invoke(event))
        }.catch {}
      }
  }

  @Suppress("UNCHECKED_CAST")
  private fun <E : Any> getBus(type: Class<E>): Bus<E> {
    return BUSES[type] as Bus<E>
  }

  @Suppress("UNCHECKED_CAST")
  private fun <E : Any> getOrPutBus(type: Class<E>): Bus<E> {
    return BUSES.getOrPut(type, { Bus<E>() }) as Bus<E>
  }

  fun <E : Any> subscribe(type: KClass<E>, func: (E) -> Any?) {
    getOrPutBus(type.java).subscribe(func)
  }

  @JvmStatic
  fun <E : Any> subscribe(type: Class<E>, func: Function<E, *>) {
    getOrPutBus(type).subscribe(func)
  }

  @JvmStatic
  fun <E : Any> subscribe(type: Class<E>, func: Consumer<E>) {
    getOrPutBus(type).subscribe(func)
  }

  fun <E : Any> unsubscribe(type: KClass<E>, func: (E) -> Any?) {
    getBus(type.java).unsubscribe(func)
  }

  @JvmStatic
  fun <E : Any> unsubscribe(type: Class<E>, func: Function<E, *>) {
    getBus(type).unsubscribe(func)
  }

  @JvmStatic
  fun <E : Any> unsubscribe(type: Class<E>, func: Consumer<E>) {
    getBus(type).unsubscribe(func)
  }
}
