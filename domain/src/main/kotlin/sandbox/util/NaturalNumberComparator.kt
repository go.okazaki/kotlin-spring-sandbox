package sandbox.util

class NaturalNumberComparator(
  private val comparator: Comparator<Comparable<Any>?>
) : Comparator<CharSequence?> {
  override fun compare(o1: CharSequence?, o2: CharSequence?): Int {

    val i1 = SplitIterator(o1)
    val i2 = SplitIterator(o2)
    var i = 0
    while (i == 0 && (i1.hasNext() || i2.hasNext())) {
      var c1: Any? = null
      var c2: Any? = null
      if (i1.hasNext()) {
        c1 = i1.next()
      }
      if (i2.hasNext()) {
        c2 = i2.next()
      }
      @Suppress("UNCHECKED_CAST")
      if (c1 is String || c2 is String) {
        i = comparator.compare(c1?.toString() as Comparable<Any>?, c2?.toString() as Comparable<Any>?)
      } else {
        i = comparator.compare(c1 as Comparable<Any>?, c2 as Comparable<Any>?)
      }
    }
    return i
  }

  private class SplitIterator(
    var source: CharSequence?
  ) : Iterator<Any?> {

    private var index = 0

    private fun length(): Int {
      return source?.length ?: 0
    }

    override fun hasNext(): Boolean {
      return index < length()
    }

    override fun next(): Any? {

      if (!hasNext()) {
        throw NoSuchElementException()
      }
      val builder = StringBuilder()
      var digit = false
      while (index < length()) {
        val c = source!![index++]
        builder.append(c)
        digit = Character.isDigit(c)
        if (index < length() && digit != Character.isDigit(source!![index])) {
          break
        }
      }
      return if (digit) {
        builder.toString().toBigDecimal()
      } else {
        val string = builder.toString().trim()
        if (string.isBlank()) {
          null
        } else {
          string
        }
      }
    }
  }
}

fun naturalNumberOrder(): NaturalNumberComparator {
  return NaturalNumberComparator(nullsLast())
}
