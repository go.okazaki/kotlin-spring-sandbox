package sandbox.util

import java.security.SecureRandom

object StringUtils {

  const val DEFAULT_STRING_LENGTH = 255

  private const val ALPHA_LOWERCASE_STRING = "abcdefghijklmnopqrstuvwxyz"
  private const val ALPHA_UPPERCASE_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
  private val SPACES_REGEX = "[\\s\\t\\n\\x0B\\f\\r\\u3000]".toRegex()

  private val RANDOM = SecureRandom()

  fun randomString(chars: CharArray, size: Int): String {

    val bytes = ByteArray(size)
    RANDOM.nextBytes(bytes)
    val len = chars.size
    val builder = StringBuilder()
    for (b in bytes) {
      val n = b.toInt()
      val i = (if (n < 0) n * -1 else n) % len
      builder.append(chars[i])
    }
    return builder.toString()
  }

  fun isNumeric(sequence: CharSequence): Boolean {
    return isNumeric(sequence.toString())
  }

  fun isNumeric(string: String): Boolean {
    return string.toIntOrNull() != null
  }

  fun isEnglish(sequence: CharSequence): Boolean {
    return sequence.filter { it in "$ALPHA_LOWERCASE_STRING $ALPHA_UPPERCASE_STRING" }.length == sequence.length
  }

  fun countMatches(sequence: CharSequence, c: Char): Int {
    return sequence.filter { c in sequence }.length
  }

  fun normalize(str: String, size: Int = DEFAULT_STRING_LENGTH): String {
    return normalizeSize(normalizeSpace(str), size)
  }

  fun normalizeSpace(str: String): String {
    return str.replace(SPACES_REGEX, "")
  }

  fun normalizeSize(str: String, size: Int = DEFAULT_STRING_LENGTH): String {
    return when {
      str.codePointCount(0, str.length) > size -> {
        val codePoints = str.codePoints().limit(size.toLong()).iterator().asSequence()
        String(codePoints.flatMap { Character.toChars(it).asSequence() }.toList().toCharArray())
      }
      else -> str
    }
  }
}
