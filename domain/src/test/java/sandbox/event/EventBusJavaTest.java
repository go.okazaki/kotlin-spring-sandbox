package sandbox.event;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.function.Function;
import org.assertj.core.api.Assertions;
import org.assertj.core.util.Strings;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;

public class EventBusJavaTest {

  @Test
  public void consumer() {

    Consumer<TestEvent> consumer1 = this::testConsumer;
    Consumer<TestEvent> consumer2 = this::testConsumer;
    Assertions.assertThat(consumer1).isNotEqualTo(consumer2);
    Assertions.assertThat(consumer1.getClass()).isNotEqualTo(consumer2.getClass());
  }

  @Test
  public void subscribe() {

    Consumer<TestEvent> consumer = this::testConsumer;
    Function<TestEvent, Boolean> func = this::testFunction;
    EventBus.subscribe(TestEvent.class, consumer);
    EventBus.subscribe(TestEvent.class, func);

    TestEvent event1 = new TestEvent("test1");
    TestEvent errorEvent = new TestEvent("");
    TestEvent event2 = new TestEvent("test2");

    List<Object> result = Flux.just(event1, errorEvent, event2)
      .flatMap(EventBus::publishParallelFlux)
      .collectList()
      .block();
    System.out.println(result);

    Assertions.assertThat(event1.getCount()).isEqualTo(2L);
    Assertions.assertThat(errorEvent.getCount()).isEqualTo(0L);
    Assertions.assertThat(event2.getCount()).isEqualTo(2L);

    EventBus.unsubscribe(TestEvent.class, consumer);
    EventBus.unsubscribe(TestEvent.class, func);

    Flux.just(event1, errorEvent, event2)
      .flatMap(EventBus::publishParallelFlux)
      .collectList()
      .block();

    Assertions.assertThat(event1.getCount()).isEqualTo(2L);
    Assertions.assertThat(errorEvent.getCount()).isEqualTo(0L);
    Assertions.assertThat(event2.getCount()).isEqualTo(2L);
  }

  public void testConsumer(TestEvent event) {
    event.touch();
  }

  public boolean testFunction(TestEvent event) {
    event.touch();
    return true;
  }

  static class TestEvent {

    private String message;

    private AtomicLong count = new AtomicLong();

    TestEvent(String message) {
      this.message = message;
    }

    public String getMessage() {
      return message;
    }

    public void touch() {

      if (Strings.isNullOrEmpty(message)) {
        throw new IllegalStateException();
      }
      System.out.println(Thread.currentThread().getName() + " " + this);
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      count.incrementAndGet();
    }

    public Long getCount() {
      return count.get();
    }

    @Override
    public String toString() {
      return "TestEvent{" +
        "message='" + message + '\'' +
        ", count=" + count +
        '}';
    }
  }
}
