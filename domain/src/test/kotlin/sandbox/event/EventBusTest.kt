package sandbox.event

import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flatMapMerge
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.launch
import kotlinx.coroutines.reactor.asCoroutineDispatcher
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import reactor.core.scheduler.Schedulers
import java.util.concurrent.atomic.LongAdder

internal class EventBusTest {

  @Test
  fun consumer() {

    val consumer1 = ::testConsumer
    val consumer2 = ::testConsumer
    Assertions.assertThat(consumer1).isEqualTo(consumer2)
    Assertions.assertThat(consumer1::class).isNotEqualTo(consumer2::class)
    Assertions.assertThat(consumer1::class.java).isNotEqualTo(consumer2::class.java)
  }

  @Test
  fun subscribe() {

    EventBus.subscribe(TestEvent::class, ::testConsumer)
    EventBus.subscribe(TestEvent::class, ::testFunction)

    val event1 = TestEvent("test1")
    val errorEvent = TestEvent("")
    val event2 = TestEvent("test2")

//    runBlocking {
//      async { EventBus.publishFlow(event1) }
//      async { EventBus.publishFlow(errorEvent) }
//      async { EventBus.publishFlow(event2) }
//    }
    val scheduler = Schedulers.elastic()
    val result = runBlocking {
      launch(scheduler.asCoroutineDispatcher()) {
        flowOf(event1, errorEvent, event2)
          .flatMapMerge { EventBus.publishFlow(it) }
          .collect()
      }.join()
    }
    println(result)

    Assertions.assertThat(event1.count.toLong()).isEqualTo(2)
    Assertions.assertThat(errorEvent.count.toLong()).isEqualTo(0)
    Assertions.assertThat(event2.count.toLong()).isEqualTo(2)

    EventBus.unsubscribe(TestEvent::class, ::testConsumer)
    EventBus.unsubscribe(TestEvent::class, ::testFunction)

    runBlocking {
      launch(scheduler.asCoroutineDispatcher()) {
        flowOf(event1, errorEvent, event2)
          .flatMapMerge { EventBus.publishFlow(it) }
          .collect()
      }.join()
    }

    Assertions.assertThat(event1.count.toLong()).isEqualTo(2)
    Assertions.assertThat(errorEvent.count.toLong()).isEqualTo(0)
    Assertions.assertThat(event2.count.toLong()).isEqualTo(2)
  }

  fun testConsumer(event: TestEvent) {
    event.touch()
  }

  fun testFunction(event: TestEvent): Boolean {
    event.touch()
    return true
  }

  data class TestEvent(
    val message: String
  ) {

    val count: LongAdder = LongAdder()

    fun touch() {

      if (message.isEmpty()) {
        throw IllegalStateException()
      }
      println(Thread.currentThread().name + " " + this)
      Thread.sleep(1000)
      count.increment()
    }
  }
}
