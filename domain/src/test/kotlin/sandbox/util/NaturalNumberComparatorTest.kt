package sandbox.util

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import java.util.Comparator.nullsLast

internal class NaturalNumberComparatorTest {

  @Test
  fun test() {

    val result = listOf("1.0.10", null, "1.0.2", "1.0.1").sortedWith(nullsLast(naturalNumberOrder()))
    Assertions.assertThat(result).containsExactly("1.0.1", "1.0.2", "1.0.10", null)
  }
}
