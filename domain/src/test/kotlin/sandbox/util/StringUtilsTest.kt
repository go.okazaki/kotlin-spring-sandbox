package sandbox.util

import org.assertj.core.api.Assertions
import org.junit.Test

internal class StringUtilsTest {

  @Test
  fun isNumeric() {

    Assertions.assertThat(StringUtils.isNumeric("")).isEqualTo(false)
    Assertions.assertThat(StringUtils.isNumeric("  ")).isEqualTo(false)
    Assertions.assertThat(StringUtils.isNumeric("123")).isEqualTo(true)
    Assertions.assertThat(StringUtils.isNumeric("\u0967\u0968\u0969")).isEqualTo(true)
    Assertions.assertThat(StringUtils.isNumeric("12 3")).isEqualTo(false)
    Assertions.assertThat(StringUtils.isNumeric("ab2c")).isEqualTo(false)
    Assertions.assertThat(StringUtils.isNumeric("12-3")).isEqualTo(false)
    Assertions.assertThat(StringUtils.isNumeric("12.3")).isEqualTo(false)
    Assertions.assertThat(StringUtils.isNumeric("-123")).isEqualTo(true)
    Assertions.assertThat(StringUtils.isNumeric("+123")).isEqualTo(true)
  }

  @Test
  fun normalizeSize1() {

    var str = "abc"
    var result = StringUtils.normalizeSize(str, 2)
    Assertions.assertThat(result).isEqualTo("ab")
  }

  @Test
  fun normalizeSize2() {

    var str = "🇯🇵"
    str.forEach {
      println(it)
    }
    var result = StringUtils.normalizeSize(str, 1)
    Assertions.assertThat(result).isEqualTo("🇯")
  }

  @Test
  fun normalize1() {

    var str = " abc "
    val result = StringUtils.normalize(str)
    Assertions.assertThat(result).isEqualTo("abc")
  }

  @Test
  fun normalize2() {

    var spaces = String(charArrayOf(' ', '　', '\t', '\r', '\n'))
    val overflow = "🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵"
    val expected = "🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯🇵🇯"
    val result = StringUtils.normalize("$spaces$overflow$spaces")
    Assertions.assertThat(result).isEqualTo(expected)
  }

}
