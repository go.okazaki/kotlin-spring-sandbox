val springBootVersion = System.getProperty("springBootVersion")
val snippetsDir = file("${buildDir}/generated-snippets")

base {
  archivesBaseName = "sandbox-web"
}

plugins {

  val kotlinVersion = System.getProperty("kotlinVersion")
  val springBootVersion = System.getProperty("springBootVersion")

  id("org.jetbrains.kotlin.plugin.spring") version kotlinVersion
  // https://plugins.gradle.org/plugin/org.springframework.boot
  id("org.springframework.boot") version springBootVersion
  // https://plugins.gradle.org/plugin/com.github.node-gradle.node
  id("com.github.node-gradle.node") version "2.2.3"
  // https://plugins.gradle.org/plugin/org.asciidoctor.jvm.convert
  id("org.asciidoctor.jvm.convert") version "3.1.0"
}

dependencies {

  // https://github.com/spring-projects/spring-boot/blob/v2.2.6.RELEASE/spring-boot-project/spring-boot-dependencies/pom.xml
  implementation(platform("org.springframework.boot:spring-boot-dependencies:${springBootVersion}"))
  runtimeOnly("org.springframework.boot:spring-boot-devtools")
  testImplementation("org.springframework.boot:spring-boot-starter-test")
  implementation("com.google.code.gson:gson:2.8.6")
//    implementation(platform("com.fasterxml.jackson:jackson-bom:2.10.3"))

  implementation("org.springframework.boot:spring-boot-starter-validation")
  implementation("org.springframework.boot:spring-boot-starter-data-mongodb-reactive")
  implementation("org.springframework:spring-jdbc")
  implementation("org.springframework.data:spring-data-r2dbc:1.0.0.RELEASE")
  // https://github.com/r2dbc/r2dbc-bom
  implementation(platform("io.r2dbc:r2dbc-bom:Arabba-RELEASE"))
  implementation("io.r2dbc:r2dbc-postgresql")
  implementation("org.flywaydb:flyway-core")
  implementation("org.postgresql:postgresql")

  implementation("org.springframework.boot:spring-boot-starter-webflux")
  implementation("org.springframework.boot:spring-boot-starter-thymeleaf")
  implementation("io.github.jpenren:thymeleaf-spring-data-dialect:3.3.1") // https://github.com/jpenren/thymeleaf-spring-data-dialect

  implementation("org.springframework.boot:spring-boot-starter-security")
  testImplementation("org.springframework.security:spring-security-test")

  implementation(project(":domain"))
  testImplementation(project(":domain", "testRuntime"))

  implementation("org.springframework.boot:spring-boot-starter-actuator")
  implementation("io.micrometer:micrometer-registry-prometheus")

  testImplementation("org.springframework.restdocs:spring-restdocs-webtestclient")
}

tasks {

  task("npmCleanInstall", com.moowork.gradle.node.npm.NpmTask::class) {
    setArgs(listOf("clean-install"))
  }

  task("buildFrontend", com.moowork.gradle.node.npm.NpmTask::class) {
    dependsOn("npmInstall")
    setArgs(listOf("run", "build"))
  }

  task("testFrontend", com.moowork.gradle.node.npm.NpmTask::class) {
    dependsOn("buildFrontend")
    setArgs(listOf("test"))
  }

//  test {
//    finalizedBy("testFrontend")
//  }

  assemble {
    dependsOn("buildFrontend")
  }

  bootJar {
    archiveFileName.set("${archiveBaseName.get()}.${archiveExtension.get()}")
  }

  asciidoctor {
    attributes(
      mapOf(
        "snippets" to "${snippetsDir}"
      )
    )
  }
}

springBoot {
  buildInfo()
}

node {
  // https://nodejs.org/ja/download/releases/
  version = "12.16.1"
  npmVersion = "6.13.4"
  download = true
}
