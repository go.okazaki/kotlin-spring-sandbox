'use strict'

import '@fortawesome/fontawesome-free/js/fontawesome'
import '@fortawesome/fontawesome-free/js/solid'
import '@fortawesome/fontawesome-free/js/regular'
import '@fortawesome/fontawesome-free/js/brands'

import 'popper.js'
import 'bootstrap'

window.jQuery = require('jquery')
window.$ = window.jQuery

$(document).ready(function() {

})

window.clearForm = function($parent) {

  if (!$parent) {
    $parent = $('form')
  }
  $parent.find('select').val(null)
  $parent.find(':checkbox').prop('checked', false)
  $parent.find(':text').val(null)
  $parent.find('.btn-group .active').removeClass('active')
}

window.cleanAlerts = function(target) {

  let $parent
  if (target) {
    $parent = $(document)
  } else {
    $parent = $(target)
  }
  let $alerts = $parent.find('[role="alert"]')
  $alerts.remove()
  $parent.find('.is-invalid').removeClass('is-invalid')
}

window.setInputValues = function($parent, json) {

  $.each(json, function(key, value) {
    $parent.find('[name="' + key + '"]').val(value).change()
  })
}

window.isValid = function(text) {

  if (0 < text.replace(/[\s\r\n\t]+/g, '').length) {
    return true
  }
  return false
}

const dateTimeFormatOptions = {
  year: 'numeric',
  month: '2-digit',
  day: '2-digit',
  hour: '2-digit',
  minute: '2-digit'
}

window.getDateTimeText = function(date) {

  if (!date) {
    date = new Date()
  }
  return date.toLocaleTimeString('ja-JP', dateTimeFormatOptions)
}

window.Progress = class Progress {

  constructor($parent) {

    this.$progress = $parent.find('.progress')
    this.init()
  }

  init() {

    this.hide()
    this.remove()
    let $bar = $('<div>')
    $bar.addClass('progress-bar')
    $bar.attr('role', 'progressbar')
    $bar.attr('aria-valuenow', '0')
    $bar.attr('aria-valuemin', '0')
    $bar.attr('aria-valuemax', '0')
    $bar.css('width: 0%;')
    $bar.text('0%')
    this.$progress.append($bar)
    return this
  }

  show() {

    this.$progress.show()
    return this
  }

  hide() {

    this.$progress.hide()
    return this
  }

  remove() {

    let $bar = this.$progress.find('.progress-bar')
    if ($bar) {
      $bar.remove()
    }
    return this
  }

  update(current, capacity) {

    let $bar = this.$progress.find('.progress-bar')
    if ($bar) {
      $bar.attr('aria-valuenow', current)
      $bar.attr('aria-valuemax', capacity)
      let percent = (current / capacity * 100).toFixed() + '%'
      $bar.css('width', percent)
      $bar.text(percent)
    }
    return this
  }

  current() {

    let current = 0
    let $bar = this.$progress.find('.progress-bar')
    if ($bar) {
      current = parseInt($bar.attr('aria-valuenow'))
    }
    return current
  }

  capacity() {

    let capacity = 0
    let $bar = this.$progress.find('.progress-bar')
    if ($bar) {
      capacity = parseInt($bar.attr('aria-valuemax'))
    }
    return capacity
  }

  isCompleted() {

    let completed = false
    if (0 < this.current() && this.current() == this.capacity()) {
      completed = true
    }
    return completed
  }

  xhr() {

    let self = this
    return function () {
      self.show()
      let xhr = $.ajaxSettings.xhr()
      xhr.onprogress = function (p) {
        if (p.lengthComputable) {
          self.update(p.loaded, p.total)
        }
      }
      xhr.upload.onprogress = function (p) {
        if (p.lengthComputable) {
          self.update(p.loaded, p.total)
        }
      }
      return xhr
    }
  }
}
