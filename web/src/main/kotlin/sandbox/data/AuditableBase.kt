package sandbox.data

import org.springframework.data.annotation.*
import org.springframework.data.domain.Auditable
import java.time.Instant
import java.util.*

abstract class AuditableBase<ID> : Auditable<String, ID, Instant>, PersistableBase<ID>() {

  @CreatedBy
  var createUser: String? = null

  @CreatedDate
  var createTime: Instant? = null

  @LastModifiedBy
  var modifyUser: String? = null

  @LastModifiedDate
  var modifyTime: Instant? = null

  @Version
  var version: Long = 0L

  override fun getCreatedBy(): Optional<String> {
    return Optional.ofNullable(createUser)
  }

  override fun setCreatedBy(createdBy: String) {
    createUser = createdBy
  }

  override fun getCreatedDate(): Optional<Instant> {
    return Optional.ofNullable(createTime)
  }

  override fun setCreatedDate(creationDate: Instant) {
    createTime = creationDate
  }

  override fun getLastModifiedBy(): Optional<String> {
    return Optional.ofNullable(modifyUser)
  }

  override fun setLastModifiedBy(lastModifiedBy: String) {
    modifyUser = lastModifiedBy
  }

  override fun getLastModifiedDate(): Optional<Instant> {
    return Optional.ofNullable(modifyTime)
  }

  override fun setLastModifiedDate(lastModifiedDate: Instant) {
    modifyTime = lastModifiedDate
  }
}
