package sandbox.data

import org.springframework.data.domain.Persistable
import java.io.Serializable

abstract class PersistableBase<ID> : Persistable<ID>, Serializable {

  override fun isNew(): Boolean {
    return id == null
  }
}
