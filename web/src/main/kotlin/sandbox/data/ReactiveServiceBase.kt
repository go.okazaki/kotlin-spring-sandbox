package sandbox.data

import org.springframework.data.repository.reactive.ReactiveCrudRepository
import org.springframework.security.core.Authentication
import reactor.core.publisher.Mono
import sandbox.security.UserAware
import java.time.Instant

abstract class ReactiveServiceBase<T, ID> {

  abstract fun getReactiveCrudRepository(): ReactiveCrudRepository<T, ID>

  open fun <S : T> save(entity: S, authentication: Authentication? = null): Mono<S> {

    if (entity is AuditableBase<*>) {
      val userAware = authentication?.details as? UserAware
      if (entity.isNew) {
        entity.createTime = Instant.now()
        if (userAware != null) {
          entity.createUser = userAware.username
        }
      }
      entity.modifyTime = Instant.now()
      if (userAware != null) {
        entity.modifyUser = userAware.username
      }
    }
    return getReactiveCrudRepository().save(entity)
  }

  open fun findById(id: ID): Mono<T> {
    return getReactiveCrudRepository().findById(id)
  }

  open fun existsById(id: ID): Mono<Boolean> {
    return getReactiveCrudRepository().existsById(id)
  }

  open fun delete(entity: T): Mono<Void> {
    return getReactiveCrudRepository().delete(entity)
  }
}
