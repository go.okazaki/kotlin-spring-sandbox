package sandbox.data.rdb

import io.r2dbc.postgresql.PostgresqlConnectionConfiguration
import io.r2dbc.postgresql.PostgresqlConnectionFactory
import io.r2dbc.spi.ConnectionFactory
import org.postgresql.ds.PGSimpleDataSource
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration
import org.springframework.data.r2dbc.connectionfactory.R2dbcTransactionManager
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories
import org.springframework.transaction.ReactiveTransactionManager
import org.springframework.transaction.annotation.EnableTransactionManagement
import javax.sql.DataSource


@Configuration
@EnableR2dbcRepositories
@EnableTransactionManagement
class RdbConfig : AbstractR2dbcConfiguration() {

  @Value("\${spring.datasource.driver-class-name:}")
  private lateinit var driverClassName: String

  @Value("\${spring.datasource.url:}")
  private lateinit var url: String

  @Value("\${postgresql.host:}")
  private lateinit var host: String

  @Value("\${postgresql.database:}")
  private lateinit var database: String

  @Value("\${postgresql.username:}")
  private lateinit var username: String

  @Value("\${postgresql.password:}")
  private lateinit var password: String

  @Bean
  fun dataSource(): DataSource {
    val dataSourceBuilder = DataSourceBuilder.create()
    dataSourceBuilder.type(PGSimpleDataSource::class.java)
    dataSourceBuilder.driverClassName(driverClassName)
    dataSourceBuilder.url(url)
    dataSourceBuilder.username(username)
    dataSourceBuilder.password(password)
    return dataSourceBuilder.build()
  }

  @Bean
  override fun connectionFactory(): ConnectionFactory {
    return PostgresqlConnectionFactory(
      PostgresqlConnectionConfiguration.builder()
        .host(host)
        .database(database)
        .username(username)
        .password(password)
        .build())
  }

  @Bean
  fun reactiveTransactionManager(connectionFactory: ConnectionFactory): ReactiveTransactionManager {
    return R2dbcTransactionManager(connectionFactory)
  }
}
