package sandbox.data.rdb

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table
import sandbox.data.AuditableBase
import sandbox.domain.Thing
import java.util.*
import kotlin.collections.LinkedHashSet
import kotlin.properties.Delegates

@Table("things")
data class ThingRecord(
  @Id
  override val thingId: UUID? = null,
  override val thingName: String = ""
) : AuditableBase<UUID>(), Thing {

  override fun getId(): UUID? {
    return thingId
  }

  override val relationIds: MutableSet<UUID> by Delegates.observable(LinkedHashSet()) { prop, old, new ->
  }
}
