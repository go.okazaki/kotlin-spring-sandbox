package sandbox.data.rdb

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table
import sandbox.data.AuditableBase
import java.util.*

@Table("users")
data class UserRecord(
  @Id
  var userId: UUID? = null,
  var username: String,
  var password: String = "",
  var email: String
) : AuditableBase<UUID>() {

  override fun getId(): UUID? {
    return userId
  }
}
