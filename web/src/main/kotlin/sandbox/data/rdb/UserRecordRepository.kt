package sandbox.data.rdb

import org.springframework.data.repository.reactive.ReactiveCrudRepository
import java.util.*

interface UserRecordRepository : ReactiveCrudRepository<UserRecord, UUID> {
}
