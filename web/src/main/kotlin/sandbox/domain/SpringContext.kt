package sandbox.domain

import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.stereotype.Component

@Component
class SpringContext : ApplicationContextAware, Context {

  private lateinit var applicationContext: ApplicationContext

  override fun setApplicationContext(applicationContext: ApplicationContext) {
    this.applicationContext = applicationContext
    Context.instance = this
  }

  override val things: Things
    get() = TODO("Not yet implemented")
}
