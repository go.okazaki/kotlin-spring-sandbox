package sandbox.domain

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.r2dbc.core.DatabaseClient
import org.springframework.data.r2dbc.core.from
import org.springframework.data.r2dbc.query.Criteria.where
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import sandbox.data.ReactiveServiceBase
import sandbox.data.rdb.UserRecord
import sandbox.data.rdb.UserRecordRepository
import java.util.*

@Service
class UserService : ReactiveServiceBase<UserRecord, UUID>() {

  @Autowired
  private lateinit var userRecordRepository: UserRecordRepository

  @Autowired
  private lateinit var databaseClient: DatabaseClient

  override fun getReactiveCrudRepository(): ReactiveCrudRepository<UserRecord, UUID> {
    return userRecordRepository
  }

  fun findByUsername(username: String): Mono<UserRecord> {
    return databaseClient.select()
      .from<UserRecord>()
      .matching(where("username").`is`(username))
      .fetch()
      .one()
  }
}
