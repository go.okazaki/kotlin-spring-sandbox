package sandbox.security

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.ReactiveUserDetailsService
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import sandbox.data.rdb.UserRecord
import sandbox.domain.UserService

@Service
class AuthenticationService : ReactiveUserDetailsService {

  @Autowired
  lateinit var userService: UserService

  @Autowired
  lateinit var passwordEncoder: PasswordEncoder

  override fun findByUsername(username: String): Mono<UserDetails> {
    return userService.findByUsername(username).map(this::toUserDetails)
  }

  private fun toUserDetails(userRecord: UserRecord): UserDetails {
    return org.springframework.security.core.userdetails.User.builder()
      .passwordEncoder(passwordEncoder::encode)
      .username(userRecord.username)
      .password(userRecord.password)
      .roles("USER")
      .build()
  }
}
