package sandbox.security

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.util.*

class UserAware(
  val userId: UUID
) : UserDetails {

  override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
    TODO("Not yet implemented")
  }

  override fun isEnabled(): Boolean {
    TODO("Not yet implemented")
  }

  override fun getUsername(): String {
    TODO("Not yet implemented")
  }

  override fun isCredentialsNonExpired(): Boolean {
    TODO("Not yet implemented")
  }

  override fun getPassword(): String {
    TODO("Not yet implemented")
  }

  override fun isAccountNonExpired(): Boolean {
    TODO("Not yet implemented")
  }

  override fun isAccountNonLocked(): Boolean {
    TODO("Not yet implemented")
  }
}
