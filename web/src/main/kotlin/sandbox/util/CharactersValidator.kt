package sandbox.util

import javax.validation.Constraint
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext
import javax.validation.ReportAsSingleViolation
import kotlin.reflect.KClass

@MustBeDocumented
@Constraint(validatedBy = [StringsValidator::class])
@Target(
  AnnotationTarget.FUNCTION,
  AnnotationTarget.FIELD,
  AnnotationTarget.ANNOTATION_CLASS,
  AnnotationTarget.PROPERTY_GETTER,
  AnnotationTarget.TYPE
)
@Retention(AnnotationRetention.RUNTIME)
@ReportAsSingleViolation
annotation class Characters(
  val type: String,
  val message: String = "{sandbox.core.util.Characters.message}",
  val groups: Array<KClass<out Any>> = [],
  val payload: Array<KClass<out Any>> = []
)

class CharactersValidator : ConstraintValidator<Characters, Any> {

  private lateinit var type: String

  override fun initialize(constraintAnnotation: Characters) {
    type = constraintAnnotation.type
  }

  override fun isValid(value: Any?, context: ConstraintValidatorContext?): Boolean {

    // TODO
    return false
  }
}
