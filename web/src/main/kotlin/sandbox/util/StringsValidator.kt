package sandbox.util

import java.util.*
import javax.validation.Constraint
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext
import javax.validation.ReportAsSingleViolation
import kotlin.reflect.KClass

@MustBeDocumented
@Constraint(validatedBy = [StringsValidator::class])
@Target(
  AnnotationTarget.FUNCTION,
  AnnotationTarget.FIELD,
  AnnotationTarget.ANNOTATION_CLASS,
  AnnotationTarget.PROPERTY_GETTER,
  AnnotationTarget.TYPE
)
@Retention(AnnotationRetention.RUNTIME)
@ReportAsSingleViolation
annotation class Strings(
  val values: Array<String>,
  val message: String = "{sandbox.core.util.Strings.message}",
  val groups: Array<KClass<out Any>> = [],
  val payload: Array<KClass<out Any>> = []
)

class StringsValidator : ConstraintValidator<Strings, Any> {

  private val strings = HashSet<String>()

  override fun initialize(constraintAnnotation: Strings) {
    strings.addAll(constraintAnnotation.values)
  }

  override fun isValid(value: Any?, context: ConstraintValidatorContext?): Boolean {

    return when (value) {
      null -> true
      is Iterable<*> -> strings.containsAll(value.toSet())
      else -> strings.contains(value)
    }
  }
}
