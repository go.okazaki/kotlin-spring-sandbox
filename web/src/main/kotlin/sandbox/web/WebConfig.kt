package sandbox.web

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.thymeleaf.dialect.springdata.SpringDataDialect
import org.thymeleaf.extras.java8time.dialect.Java8TimeDialect

@Configuration
class WebConfig {

  @Bean
  fun java8TimeDialect(): Java8TimeDialect {
    return Java8TimeDialect()
  }

  @Bean
  fun springDataDialect(): SpringDataDialect {
    return SpringDataDialect()
  }

}
