LOAD 'auto_explain';
SET auto_explain.log_analyze = on;
SET auto_explain.log_min_duration = 500;
SET log_min_duration_statement = 3000;

--ALTER SYSTEM SET shared_preload_libraries = "auto_explain";
--ALTER SYSTEM SET auto_explain.log_analyze = on;
--ALTER SYSTEM SET auto_explain.log_min_duration = 500;
--ALTER SYSTEM SET log_min_duration_statement = 3000;
