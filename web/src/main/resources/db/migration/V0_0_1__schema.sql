CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE public.users (
	user_id uuid NOT NULL DEFAULT uuid_generate_v4(),
	username varchar(255) NOT NULL,
	password varchar(255) NOT NULL,
	email varchar(255) NOT NULL,
	create_user varchar(255),
	create_time timestamp,
	modify_user varchar(255),
	modify_time timestamp,
	version bigint,
	CONSTRAINT users_pk PRIMARY KEY (user_id),
	CONSTRAINT users_un_username UNIQUE (username),
	CONSTRAINT users_un_email UNIQUE (email)
);

CREATE TABLE public.things (
	thing_id uuid NOT NULL DEFAULT uuid_generate_v4(),
	thing_name varchar(255) NOT NULL,
	create_user varchar(255),
  create_time timestamp,
  modify_user varchar(255),
  modify_time timestamp,
	version bigint,
	CONSTRAINT things_pk PRIMARY KEY (thing_id)
);
