describe('main', function(){
    it('isValid', function(){
        expect(isValid('')).toEqual(false)
        expect(isValid(' ')).toEqual(false)
        expect(isValid('　')).toEqual(false)
        expect(isValid('\n')).toEqual(false)
        expect(isValid('\r')).toEqual(false)
        expect(isValid('\r\n')).toEqual(false)
        expect(isValid('\t')).toEqual(false)
        expect(isValid(' \n')).toEqual(false)

        expect(isValid('a')).toEqual(true)
        expect(isValid('a\n')).toEqual(true)
        expect(isValid('\na')).toEqual(true)
    });
});
