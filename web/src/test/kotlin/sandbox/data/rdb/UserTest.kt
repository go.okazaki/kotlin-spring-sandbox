package sandbox.core.domain

import kotlinx.coroutines.reactor.mono
import org.assertj.core.api.Assertions
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.ReactiveTransactionManager
import org.springframework.transaction.reactive.TransactionalOperator
import reactor.core.publisher.Mono
import sandbox.data.rdb.UserRecord
import sandbox.domain.UserService

@RunWith(SpringRunner::class)
@SpringBootTest
internal class UserTest {

  @Autowired
  lateinit var userService: UserService

  @Autowired
  lateinit var reactiveTransactionManager: ReactiveTransactionManager

  @Test
  fun crud() {

    var user = UserRecord(username = "user1", email = "user1@test.local")
    user = userService.save(user).block()!!
    Assertions.assertThat(user.userId).isNotNull()
//    Assertions.assertThat(user.createTime).isNotNull()

    val newname = "user1 tested"
    user.username = newname
    userService.save(user).block()

    var result = userService.findById(user.userId!!).block()
    Assertions.assertThat(result!!.username).isEqualTo(newname)

    userService.delete(user).block()
    result = userService.findById(user.userId!!).block()
    Assertions.assertThat(result).isNull()
  }

  @Test
  fun reactor() {

    val transactionalOperator = TransactionalOperator.create(reactiveTransactionManager)
    Mono.just(UserRecord(username = "user2", email = "user2@test.local"))
      .flatMap { user -> userService.save(user) }
      .flatMap { user ->
        val newname = "user2 tested"
        user.username = newname
        userService.save(user)
      }
      .flatMap { user ->
        userService.findById(user.userId!!)
      }
      .flatMap { user ->
        userService.delete(user!!)
      }
      .`as`(transactionalOperator::transactional)
      .block()
  }

  @Test
  fun coroutine() {

    val transactionalOperator = TransactionalOperator.create(reactiveTransactionManager)
    mono { UserRecord(username = "user3", email = "user3@test.local") }
      .flatMap { user -> userService.save(user) }
      .flatMap { user ->
        val newname = "user3 tested"
        user.username = newname
        userService.save(user)
      }
      .flatMap { user ->
        userService.findById(user.userId!!)
      }
      .flatMap { user ->
        userService.delete(user!!)
      }
      .`as`(transactionalOperator::transactional)
      .block()
  }
}
