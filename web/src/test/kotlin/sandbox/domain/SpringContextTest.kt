package sandbox.domain

import org.assertj.core.api.Assertions
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
internal class SpringContextTest {

  @Test
  fun instance() {

    val context = Context.instance
    Assertions.assertThat(context).isInstanceOf(SpringContext::class.java)
  }
}
