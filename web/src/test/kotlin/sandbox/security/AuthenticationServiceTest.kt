package sandbox.security

import org.assertj.core.api.Assertions
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import sandbox.data.rdb.UserRecord
import sandbox.domain.UserService

@RunWith(SpringRunner::class)
@SpringBootTest
internal class AuthenticationServiceTest {

  @Autowired
  lateinit var authenticationService: AuthenticationService

  @Autowired
  lateinit var userService: UserService

  lateinit var userRecord: UserRecord

  @Before
  fun setUp() {
    val username = "user1"
    userRecord = userService.findByUsername(username)
      .switchIfEmpty(userService.save(UserRecord(username = username, email = "user1@test.local")))
      .block()!!
  }

  @After
  fun tearDown() {
    userService.delete(userRecord).block()
  }

  @Test
  fun findByUsername() {

    val userdetails = authenticationService.findByUsername(userRecord.username).block()
    Assertions.assertThat(userdetails!!.password).isNotNull()
  }
}
