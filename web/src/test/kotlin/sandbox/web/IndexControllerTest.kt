package sandbox.web

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.reactive.server.WebTestClient

@RunWith(SpringRunner::class)
@WebFluxTest
@Import(WebTestConfig::class)
class IndexControllerTest {

  @Autowired
  lateinit var webTestClient: WebTestClient

  @WithMockUser
  @Test
  fun test() {

    webTestClient.get().uri("/").accept(MediaType.TEXT_HTML)
      .exchange()
      .expectStatus()
      .isOk()
  }
}
