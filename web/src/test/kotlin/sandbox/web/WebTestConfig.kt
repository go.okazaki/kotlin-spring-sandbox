package sandbox.web

import org.junit.Rule
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.restdocs.JUnitRestDocumentation
import org.springframework.restdocs.webtestclient.WebTestClientRestDocumentation
import org.springframework.web.reactive.function.client.ExchangeFilterFunction

@TestConfiguration
class WebTestConfig {

  @Rule
  val restDocumentation = JUnitRestDocumentation()

  @Bean
  fun webTestClientRestDocumentationFilter(): ExchangeFilterFunction {
    return WebTestClientRestDocumentation.documentationConfiguration(restDocumentation)
  }
}
